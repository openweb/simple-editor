module.exports = {
    configureWebpack: {        
        output: {
            filename: 'simple-editor.min.js'
        },
        optimization: {
            splitChunks: false
        }
    },
    chainWebpack: config => {
        if (config.plugins.has("extract-css")) {
          const extractCSSPlugin = config.plugin("extract-css");
          extractCSSPlugin &&
            extractCSSPlugin.tap(() => [
              {
                filename:  "simple-editor.min.css",
              }
            ]);
        }
        config.plugins
         //   .delete("html")
            .delete("prefetch")
            .delete("preload");
      },
    
    filenameHashing: false
}
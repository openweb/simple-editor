import Vue from "vue";
import App from "./components/Editor.vue";
import $ from 'jquery';

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faBold,
  faItalic,
  faUnderline,
  faStrikethrough,
  faAlignLeft,
  faAlignCenter,
  faAlignRight

} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faBold, faItalic, faUnderline, faStrikethrough, faAlignLeft, faAlignCenter, faAlignRight);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;


Object.values(document.querySelectorAll("input[type=simple-editor]")).forEach(el => {
  const name = el.getAttribute('name');
  const value = el.getAttribute('value');
  new Vue({
    render: h => h(App,{
      props: {
        value,
        name
      }
    })
  }).$mount(el);
});

$(document).on('change', '.simple-editor input', function() {
  var event = new Event('input', {
      bubbles: true,
      cancelable: true,
  });

  var el = $(this)[0];

  el.dispatchEvent(event);
});